﻿using AarhusWebDevCoop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Mvc;

namespace AarhusWebDevCoop.Controllers
{
    public class TestFormSurfaceController : SurfaceController
    {
        // GET: TestFormSurface
        public ActionResult Index()
        {
            return PartialView("TestForm", new TestFormViewModel());
        }
    }
}