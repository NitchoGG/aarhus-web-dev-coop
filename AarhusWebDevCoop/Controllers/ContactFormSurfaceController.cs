﻿using AarhusWebDevCoop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using Umbraco.Core.Models;

namespace AarhusWebDevCoop.Controllers
{
    public class ContactFormSurfaceController : SurfaceController
    {
        // Vi starter med at returne et view som er baseret på min ViewModel. Min ViewModel indeholder ikke så meget andet en de værdier som jeg har i mine input felter, så altså
        //Name, Email, Subject og en Besked. Alle felterne er required på email sikre jeg mig at det er en valid email
        public ActionResult Index()
        {
            return PartialView("ContactForm", new ContactFormViewModel());
        }

        //Jeg laver derefter et ActionResult som igen er baseret på min View Model og den bliver brugt når vi klikker på submit knappe
        [HttpPost]
        public ActionResult HandleFormSubmit(ContactFormViewModel model)
        {
            //Vi starter med at kigge på om vores ModelState er valid, hvis den ikke er, så return vi bare vores page
            if (!ModelState.IsValid)
            {
                return CurrentUmbracoPage();
            }

            //Jeg sætter derefter et mail message system op, hvor jeg bruge emailen fra min model, jeg tager emnet fra min model, jeg tager navnet fra min model og til sidst beskeden man vil sende, hvilket også bliver taget fra min model
            MailMessage message = new MailMessage();
            message.To.Add("nitchogg@gmail.com");
            message.Subject = model.Subject;
            message.From = new MailAddress(model.Email, model.Name);
            message.Body = model.Message;

            //Jeg bruger derefter en Simple Mail Transfer hvor jeg bruger min egen mail, 
            using (SmtpClient smtp = new SmtpClient())
            {
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.UseDefaultCredentials = false;
                smtp.EnableSsl = true;
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;
                smtp.Credentials = new System.Net.NetworkCredential("nitchogg@gmail.com", "09az09azschmidt");

                // Jeg sender derefter den beskedet vi lavede før
                smtp.Send(message);
                TempData["success"] = true;
            }

            //Når Beskeden er blevet sendt på mail, så sæger vi også for at i vores backoffice under Content > Message og oprette den der. Det gør jeg vha. dette kode
            // Jeg burger IContent til at lave den comment jeg vil lave, derefter bruger jeg CreateContent til at oprette den, hvor jeg giver den Emnet fra min model, ID'et fra min side og hvilket content node den skal være under, hvilket er Message
            IContent comment = Services.ContentService.CreateContent(model.Subject, CurrentPage.Id, "Message");

            //Jeg sætter derefter alle værdierne udfra min view model
            comment.SetValue("messageName", model.Name);
            comment.SetValue("email", model.Email);
            comment.SetValue("subject", model.Subject);
            comment.SetValue("messageContent", model.Message);


            //Jeg gemmer derefter, uden at publish, da man kan have en editor til at gøre det istedet.
            Services.ContentService.Save(comment);

            //Services.ContentService.SaveAndPublishWithStatus(comment);

            //Til sidst redirected jeg tilbage til den current umbraco side.
            return RedirectToCurrentUmbracoPage();
        }

    }
}