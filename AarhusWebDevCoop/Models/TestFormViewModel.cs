﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AarhusWebDevCoop.Models
{
    public class TestFormViewModel
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Message { get; set; }
    }
}