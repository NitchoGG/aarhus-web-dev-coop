﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AarhusWebDevCoop.Models
{
    public class ContactFormViewModel
    {
        [Required(ErrorMessage ="Please Enter your name")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Please Enter your Email")]
        [Display(Name="Email")]
        [RegularExpression(@"[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$", ErrorMessage ="Please enter a correct email address")]
        public string Email { get; set; }
        [Required(ErrorMessage ="Please enter a subject")]
        public string Subject { get; set; }
        [Required(ErrorMessage ="Please enter a message")]
        public string Message { get; set; }
    }
}